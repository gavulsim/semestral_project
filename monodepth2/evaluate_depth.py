from __future__ import absolute_import, division, print_function

import os
import cv2
import numpy as np
import matplotlib as mpl
import matplotlib.cm as cm
import PIL.Image as pil
import pickle
import csv

import torch
from torch.utils.data import DataLoader

from layers import disp_to_depth
from utils import readlines
from options import MonodepthOptions
import datasets
import networks

from utils import save_image_to_file
from utils import crop_gt_depth

cv2.setNumThreads(0)  # This speeds up evaluation 5x on our unix systems (OpenCV 3.3.1)


current_dir = os.path.dirname(__file__)
splits_dir = os.path.join(current_dir, "splits")
output_dir = current_dir.replace("/monodepth2", "/output_data")

# Models which were trained with stereo supervision were trained with a nominal
# baseline of 0.1 units. The KITTI rig has a baseline of 54cm. Therefore,
# to convert our stereo predictions to real-world scale we multiply our depths by 5.4.
STEREO_SCALE_FACTOR = 5.4

def compute_errors(gt, pred):
    """Computation of error metrics between predicted and ground truth depths
    """
    thresh = np.maximum((gt / pred), (pred / gt))
    a1 = (thresh < 1.25     ).mean()
    a2 = (thresh < 1.25 ** 2).mean()
    a3 = (thresh < 1.25 ** 3).mean()

    rmse = (gt - pred) ** 2
    rmse = np.sqrt(rmse.mean())

    rmse_log = (np.log(gt) - np.log(pred)) ** 2
    rmse_log = np.sqrt(rmse_log.mean())

    abs_rel = np.mean(np.abs(gt - pred) / gt)

    sq_rel = np.mean(((gt - pred) ** 2) / gt)

    return abs_rel, sq_rel, rmse, rmse_log, a1, a2, a3


def batch_post_process_disparity(l_disp, r_disp):
    """Apply the disparity post-processing method as introduced in Monodepthv1
    """
    _, h, w = l_disp.shape
    m_disp = 0.5 * (l_disp + r_disp)
    l, _ = np.meshgrid(np.linspace(0, 1, w), np.linspace(0, 1, h))
    l_mask = (1.0 - np.clip(20 * (l - 0.05), 0, 1))[None, ...]
    r_mask = l_mask[:, :, ::-1]
    return r_mask * l_disp + l_mask * r_disp + (1.0 - l_mask - r_mask) * m_disp

def save_paths_of_imgs_to_list(opt, data, dataset, img_paths):
    if opt.dataset_type == "kitti":
        folder = data[("folder", 0)]
        frame_index = data[("frame_index", 0)]
        side = data[("side", 0)]
        # Loop through batch of images
        for i in range(len(folder)):
            img_path = dataset.get_image_path(folder[i], frame_index[i], side[i])
            img_paths.append(img_path)
    elif opt.dataset_type == "cityscapes":
        first_folder = data[("first_folder", 0)]
        city_folder = data[("city_folder", 0)]
        image_name = data[("image_name", 0)]
        # Loop through batch of images
        for i in range(len(first_folder)):
            img_path = dataset.get_image_path([first_folder[i], city_folder[i], image_name[i]])
            img_paths.append(img_path)
    
    img = cv2.imread(img_paths[0])
    # print("Image path 0: ", img_paths[0])
    return img_path

def load_pickle_gt(opt):
    gt_path = os.path.join(splits_dir, opt.eval_split, "gt_depths.pkl")
    with open(gt_path, 'rb') as f:
        gt_depths = pickle.load(f)
    return gt_depths

def load_numpy_gt(opt):
    gt_path = os.path.join(splits_dir, opt.eval_split, "gt_depths.npz")
    gt_depths = np.load(gt_path, fix_imports=True, encoding='latin1')["data"]
    return gt_depths

def save_errors_to_file(mean_errors, dataset_type, crop_factor, crop_type, dataset_split):
    crop_factor_array = np.array([crop_factor])
    errors_with_crop = np.concatenate((crop_factor_array, mean_errors), axis=None)
    output_dir_crop = os.path.join(output_dir, f"crop_{crop_type}")
    output_matrices_path = f"{output_dir_crop}/raw_matrices/{dataset_type}"
    file_error = f"{dataset_type}_errors.csv"
    if dataset_split == "test_files":
        error_path = os.path.join(output_dir_crop, file_error)
    else:
        error_path = os.path.join(output_matrices_path, file_error)
    with open(error_path, mode='a', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(errors_with_crop)

def save_abs_diff_to_file(dataset_split, crop_type, dataset_type, crop_factor, errors):
    if dataset_split == "test_file":
        return
    file_error_path = f"{output_dir}/crop_{crop_type}/raw_matrices/{dataset_type}/{dataset_type}_{crop_factor}_all_errors.csv"
    with open(file_error_path, "w", newline='') as file_abs_diff:
       writer = csv.writer(file_abs_diff)
       for err in errors:
           writer.writerow([err[1]])

def evaluate(opt):
    """Evaluates a pretrained model using a specified test set
    """
    # Parameters
    MIN_DEPTH = 1e-3
    MAX_DEPTH = 80
    NUM_OF_SAVE_IMAGES = 0

    assert sum((opt.eval_mono, opt.eval_stereo)) == 1, \
        "Please choose mono or stereo evaluation by setting either --eval_mono or --eval_stereo"

    if opt.ext_disp_to_eval is None:
        opt.load_weights_folder = os.path.expanduser(opt.load_weights_folder)

        assert os.path.isdir(opt.load_weights_folder), \
            "Cannot find a folder at {}".format(opt.load_weights_folder)

        print("-> Loading weights from {}".format(opt.load_weights_folder))

        dataset_file_name = opt.dataset_split + ".txt"
        filenames = readlines(os.path.join(splits_dir, opt.eval_split, dataset_file_name))
        # filenames --> list of lines in test_files.txt
        # print("Filenames path: ", os.path.join(splits_dir, opt.eval_split, dataset_file_name))
        # --> Filenames path: /home.nfs/gavulsim/monodepth2/splits/eigen_benchmark/test_files.txt
        
        encoder_path = os.path.join(opt.load_weights_folder, "encoder.pth")
        decoder_path = os.path.join(opt.load_weights_folder, "depth.pth")

        encoder_dict = torch.load(encoder_path)

        if opt.dataset_type == "kitti":
            dataset = datasets.KITTIRAWDataset(opt, opt.crop_factor, opt.crop_type, opt.data_path, opt.dataset_type, filenames,
                                               encoder_dict['height'], encoder_dict['width'],
                                               [0], 4, is_train=False)
        elif opt.dataset_type == "cityscapes":
            dataset = datasets.CityscapesDataset(opt, opt.crop_factor, opt.crop_type, opt.data_path, opt.dataset_type, filenames,
                                               encoder_dict['height'], encoder_dict['width'],
                                               [0], 4, is_train=False)
        
        dataloader = DataLoader(dataset, 16, shuffle=False, num_workers=opt.num_workers,
                                pin_memory=True, drop_last=False)
        
        encoder = networks.ResnetEncoder(opt.num_layers, False)
        depth_decoder = networks.DepthDecoder(encoder.num_ch_enc)

        model_dict = encoder.state_dict()
        encoder.load_state_dict({k: v for k, v in encoder_dict.items() if k in model_dict})
        depth_decoder.load_state_dict(torch.load(decoder_path))

        encoder.cuda()
        encoder.eval()
        depth_decoder.cuda()
        depth_decoder.eval()

        pred_disps = []
        disps = []
        img_paths = []

        print("-> Computing predictions with size {}x{}".format(
            encoder_dict['width'], encoder_dict['height']))

        with torch.no_grad():
            for i, data in enumerate(dataloader):
                input_color = data[("color", 0, 0)].cuda()
                if i == 0:
                    print("Input color size: ", input_color.shape)
                    # print("Input color: ", input_color[0])
                    # torch.save(input_color[0], f"{output_dir}/input_color_tfs.pt")
                
                save_paths_of_imgs_to_list(opt, data, dataset, img_paths)

                if opt.post_process:
                    # Post-processed results require each image to have two forward passes
                    input_color = torch.cat((input_color, torch.flip(input_color, [3])), 0)

                output = depth_decoder(encoder(input_color))
                if i == 0:
                    print("Output size: ", output[("disp", 0)].shape)
                    # print("Output: ", output[("disp", 0)][0])
                    # torch.save(output[("disp", 0)][0], f"{output_dir}/output_tfs.pt")
                disp = output[("disp", 0)]

                for item in disp:
                    disps.append(item)
                
                pred_disp, _ = disp_to_depth(disp, opt.min_depth, opt.max_depth)
                pred_disp = pred_disp.cpu()[:, 0].numpy()

                if opt.post_process:
                    N = pred_disp.shape[0] // 2
                    pred_disp = batch_post_process_disparity(pred_disp[:N], pred_disp[N:, :, ::-1])

                pred_disps.append(pred_disp)

        pred_disps = np.concatenate(pred_disps)

        print(f"Prediction size: {len(pred_disps[0])}x{len(pred_disps[0][0])}")
    else:
        # Load predictions from file
        print("-> Loading predictions from {}".format(opt.ext_disp_to_eval))
        pred_disps = np.load(opt.ext_disp_to_eval)

        if opt.eval_eigen_to_benchmark:
            eigen_to_benchmark_ids = np.load(
                os.path.join(splits_dir, "benchmark", "eigen_to_benchmark_ids.npy"))

            pred_disps = pred_disps[eigen_to_benchmark_ids]

    if opt.save_pred_disps:
        output_path = os.path.join(
            opt.load_weights_folder, "disps_{}_split.npy".format(opt.eval_split))
        print("-> Saving predicted disparities to ", output_path)
        np.save(output_path, pred_disps)

    if opt.no_eval:
        print("-> Evaluation disabled. Done.")
        quit()

    elif opt.eval_split == 'benchmark':
        save_dir = os.path.join(opt.load_weights_folder, "benchmark_predictions")
        print("-> Saving out benchmark predictions to {}".format(save_dir))
        if not os.path.exists(save_dir):
            os.makedirs(save_dir)

        for idx in range(len(pred_disps)):
            disp_resized = cv2.resize(pred_disps[idx], (1216, 352))
            depth = STEREO_SCALE_FACTOR / disp_resized
            depth = np.clip(depth, 0, 80)
            depth = np.uint16(depth * 256)
            save_path = os.path.join(save_dir, "{:010d}.png".format(idx))            
            cv2.imwrite(save_path, depth)

        print("-> No ground truth is available for the KITTI benchmark, so not evaluating. Done.")
        quit()

    # Numpy data loading
    # gt_depths = load_numpy_gt(opt)
    # Pickle data loading
    gt_depths = load_pickle_gt(opt)
    
    print("-> Evaluating")

    if opt.eval_stereo:
        print("   Stereo evaluation - "
              "disabling median scaling, scaling by {}".format(STEREO_SCALE_FACTOR))
        opt.disable_median_scaling = True
        opt.pred_depth_scale_factor = STEREO_SCALE_FACTOR
    else:
        print("   Mono evaluation - using median scaling")

    errors = []
    ratios = []
    
    for i in range(pred_disps.shape[0]):
        # Get depth
        gt_depth = gt_depths[i][0]
        # Get dimesions of the ground truth depth
        gt_height_original, gt_width_original = gt_depths[i][1]
        gt_height, gt_width = gt_depth.shape[:2]

        max_crop_w = int(gt_width_original * 0.2)
        max_crop_width = gt_width_original - (2 * max_crop_w)
        original_mask = np.zeros((gt_height_original, gt_width_original), dtype=bool)
        original_mask[:, max_crop_w:(gt_width_original - max_crop_w)] = True

        cropped_mask, _ = crop_gt_depth(original_mask, opt.crop_factor, opt.crop_type, i)

        # Get disparity
        pred_disp = pred_disps[i]
        if i == 0:
            print("Ground truth size: ", gt_depth.shape)
            print("Prediction size: ", pred_disp.shape)
        # Resize disparity
        pred_disp = cv2.resize(pred_disp, (gt_width, gt_height))
        # Convert disparity to predicted depth
        pred_depth = 1 / pred_disp

        if opt.eval_split == "eigen":
            # Crop the image
            mask = np.logical_and(gt_depth > MIN_DEPTH, gt_depth < MAX_DEPTH)
            crop = np.array([0.40810811 * gt_height, 0.99189189 * gt_height,
                             0.03594771 * gt_width,  0.96405229 * gt_width]).astype(np.int32)
            crop_mask = np.zeros(mask.shape, dtype=bool)
            crop_mask[crop[0]:crop[1], crop[2]:crop[3]] = True
            mask = np.logical_and(mask, crop_mask)
        else:
            mask = gt_depth > 0
        
        ratio = np.median(gt_depth[mask]) / np.median(pred_depth[mask])
        pred_depth_scaled = pred_depth * ratio
        
        if i == 0:
            # Save cropped mask to file
            save_image_to_file(cropped_mask, "mask_matrix", opt.dataset_type, opt.crop_type, opt.dataset_split)
            # Save predicted depth to file
            save_image_to_file(pred_depth, "pred_depth_tfs", opt.dataset_type, opt.crop_type, opt.dataset_split)
            # Compute scaled prediction depth
            pred_depth_scaled[pred_depth_scaled < MIN_DEPTH] = MIN_DEPTH
            pred_depth_scaled[pred_depth_scaled > MAX_DEPTH] = MAX_DEPTH
            save_image_to_file(pred_depth_scaled, "pred_depth_scaled", opt.dataset_type, opt.crop_type, opt.dataset_split)
            # Compute error matrix relative
            error_matrix_relative = np.where(mask == True, np.abs(gt_depth - pred_depth_scaled) / gt_depth, -1)
            save_image_to_file(error_matrix_relative, "error_matrix", opt.dataset_type, opt.crop_type, opt.dataset_split)
            save_image_to_file(error_matrix_relative, f"error_matrices/error_matrix_relative_{opt.crop_factor}", opt.dataset_type, opt.crop_type, opt.dataset_split)
            # valid_error_values = error_matrix_relative[error_matrix_relative != -1]
            # error_relative_mean = np.mean(valid_error_values)
            # print("Error relative mean: ", error_relative_mean)

        cropped_mask = np.logical_and(mask, cropped_mask)
        error_matrix_relative_cropped = np.where(cropped_mask == True, np.abs(gt_depth - pred_depth_scaled) / gt_depth, -1)
        valid_error_crop_values = error_matrix_relative_cropped[error_matrix_relative_cropped != -1]
        error_relative_crop_mean = np.mean(valid_error_crop_values)
        # print("Error relative mean: ", error_relative_crop_mean)
        if i == 0:
            save_image_to_file(error_matrix_relative_cropped, "error_matrix_crop", opt.dataset_type, opt.crop_type, opt.dataset_split)
            err_mat_relative_crop = np.abs(error_matrix_relative.shape[1] - max_crop_width)
            err_matrix_relative_crop_width = int(err_mat_relative_crop / 2)
            error_matrix_relative_cropped = error_matrix_relative_cropped[:, err_matrix_relative_crop_width:(gt_width - err_matrix_relative_crop_width)]
            save_image_to_file(error_matrix_relative_cropped, f"error_matrices/error_matrix_relative_crop_{opt.crop_factor}", opt.dataset_type, opt.crop_type, opt.dataset_split)

        pred_depth = pred_depth[mask]
        gt_depth = gt_depth[mask]

        # pred_depth_scale_factor default is 1
        pred_depth *= opt.pred_depth_scale_factor
        if not opt.disable_median_scaling:
            ratio = np.median(gt_depth) / np.median(pred_depth)
            ratios.append(ratio)
            pred_depth *= ratio
        
        pred_depth[pred_depth < MIN_DEPTH] = MIN_DEPTH
        pred_depth[pred_depth > MAX_DEPTH] = MAX_DEPTH

        abs_rel, sq_rel, rmse, rmse_log, a1, a2, a3 = compute_errors(gt_depth, pred_depth)

        errors.append((error_relative_crop_mean, abs_rel, sq_rel, rmse, rmse_log, a1, a2, a3))

    if not opt.disable_median_scaling:
        ratios = np.array(ratios)
        med = np.median(ratios)
        print(" Scaling ratios | med: {:0.3f} | std: {:0.3f}".format(med, np.std(ratios / med)))

    save_abs_diff_to_file(opt.dataset_split, opt.crop_type, opt.dataset_type, opt.crop_factor, errors)
    mean_errors = np.array(errors).mean(0)
    print("\n  " + ("{:>8} | " * 8).format("crop_abs_rel", "abs_rel", "sq_rel", "rmse", "rmse_log", "a1", "a2", "a3"))
    print(("&{: 8.3f}  " * 8).format(*mean_errors.tolist()) + "\\\\")
    print("\n-> Done!")
    save_errors_to_file(mean_errors, opt.dataset_type, opt.crop_factor, opt.crop_type, opt.dataset_split)

if __name__ == "__main__":
    options = MonodepthOptions()
    evaluate(options.parse())
