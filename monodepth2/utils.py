# Copyright Niantic 2019. Patent Pending. All rights reserved.
#
# This software is licensed under the terms of the Monodepth2 licence
# which allows for non-commercial use only, the full terms of which are made
# available in the LICENSE file.

from __future__ import absolute_import, division, print_function
import os
import hashlib
import zipfile
import numpy as np
from six.moves import urllib
import torch
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.cm as cm
from enum import Enum

class Datasets(Enum):
    KITTI = "kitti"
    CITY = "cityscapes"

FOCAL_LENGTH = 0.209313
BASELINE = 2262.52
MAIN_DIR = os.path.dirname(os.path.dirname(__file__))

def readlines(filename):
    """Read all the lines in a text file and return as a list
    """
    with open(filename, 'r') as f:
        lines = f.read().splitlines()
    return lines


def normalize_image(x):
    """Rescale image pixels to span range [0, 1]
    """
    ma = float(x.max().cpu().data)
    mi = float(x.min().cpu().data)
    d = ma - mi if ma != mi else 1e5
    return (x - mi) / d


def sec_to_hm(t):
    """Convert time in seconds to time in hours, minutes and seconds
    e.g. 10239 -> (2, 50, 39)
    """
    t = int(t)
    s = t % 60
    t //= 60
    m = t % 60
    t //= 60
    return t, m, s


def sec_to_hm_str(t):
    """Convert time in seconds to a nice string
    e.g. 10239 -> '02h50m39s'
    """
    h, m, s = sec_to_hm(t)
    return "{:02d}h{:02d}m{:02d}s".format(h, m, s)


def download_model_if_doesnt_exist(model_name):
    """If pretrained kitti model doesn't exist, download and unzip it
    """
    # values are tuples of (<google cloud URL>, <md5 checksum>)
    download_paths = {
        "mono_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono_640x192.zip",
             "a964b8356e08a02d009609d9e3928f7c"),
        "stereo_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/stereo_640x192.zip",
             "3dfb76bcff0786e4ec07ac00f658dd07"),
        "mono+stereo_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono%2Bstereo_640x192.zip",
             "c024d69012485ed05d7eaa9617a96b81"),
        "mono_no_pt_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono_no_pt_640x192.zip",
             "9c2f071e35027c895a4728358ffc913a"),
        "stereo_no_pt_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/stereo_no_pt_640x192.zip",
             "41ec2de112905f85541ac33a854742d1"),
        "mono+stereo_no_pt_640x192":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono%2Bstereo_no_pt_640x192.zip",
             "46c3b824f541d143a45c37df65fbab0a"),
        "mono_1024x320":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono_1024x320.zip",
             "0ab0766efdfeea89a0d9ea8ba90e1e63"),
        "stereo_1024x320":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/stereo_1024x320.zip",
             "afc2f2126d70cf3fdf26b550898b501a"),
        "mono+stereo_1024x320":
            ("https://storage.googleapis.com/niantic-lon-static/research/monodepth2/mono%2Bstereo_1024x320.zip",
             "cdc5fc9b23513c07d5b19235d9ef08f7"),
        }

    if not os.path.exists("models"):
        os.makedirs("models")

    model_path = os.path.join("models", model_name)

    def check_file_matches_md5(checksum, fpath):
        if not os.path.exists(fpath):
            return False
        with open(fpath, 'rb') as f:
            current_md5checksum = hashlib.md5(f.read()).hexdigest()
        return current_md5checksum == checksum

    # see if we have the model already downloaded...
    if not os.path.exists(os.path.join(model_path, "encoder.pth")):

        model_url, required_md5checksum = download_paths[model_name]

        if not check_file_matches_md5(required_md5checksum, model_path + ".zip"):
            print("-> Downloading pretrained model to {}".format(model_path + ".zip"))
            urllib.request.urlretrieve(model_url, model_path + ".zip")

        if not check_file_matches_md5(required_md5checksum, model_path + ".zip"):
            print("   Failed to download a file which matches the checksum - quitting")
            quit()

        print("   Unzipping model...")
        with zipfile.ZipFile(model_path + ".zip", 'r') as f:
            f.extractall(model_path)

        print("   Model unzipped to {}".format(model_path))

def convert_disp_to_depth(img_d):
    # Normalize the disparity map
    img_d[img_d > 0] = (img_d[img_d > 0] - 1) / 256
    depth_map = np.zeros_like(img_d)
    # Create mask with valid values that are greater than zero
    valid_disp = img_d > 0
    # Compute depth map with formula depth = (focal_length * baseline) / disparity
    depth_map[valid_disp] = (FOCAL_LENGTH * BASELINE) / img_d[valid_disp]
    return depth_map

def resize_gt_depth(gt_depth, size, i):
    if i == 0:
        print(f"Resizing image")

    # Convert the depth map to a PyTorch tensor
    gt_depth_tensor = torch.from_numpy(gt_depth).unsqueeze(0).unsqueeze(0)
    # Resize the depth map using torch.nn.functional.interpolate
    gt_depth_resized = torch.nn.functional.interpolate(gt_depth_tensor, size=size, mode='nearest')
    # Convert back to NumPy array
    gt_depth = gt_depth_resized.squeeze(0).squeeze(0).cpu().numpy()
    return gt_depth
    
def crop_gt_depth(original_gt_depth, crop_factor, crop_type, i):
    if i == 0:
        print(f"Cropping with Crop factor: {crop_factor}")

    # Get height and width of original depth map
    gt_depth_height = original_gt_depth.shape[0]
    gt_depth_width = original_gt_depth.shape[1]
    size = (gt_depth_height, gt_depth_width)
    
    # Calculate the amount to crop
    crop_h = int(gt_depth_height * crop_factor)
    crop_w = int(gt_depth_width * crop_factor)
    
    # Crop the image
    # Crop whole image
    # cropped_gt_depth = original_gt_depth[crop_h:gt_depth_height - crop_h, crop_w:gt_depth_width - crop_w]
    if crop_type == "vertical":
        # Crop image height
        cropped_gt_depth = original_gt_depth[crop_h:gt_depth_height - crop_h, :]
    elif crop_type == "horizontal":
        # Crop image width
        cropped_gt_depth = original_gt_depth[:, crop_w:gt_depth_width - crop_w]
    return cropped_gt_depth, size

def save_resized_and_original_image(gt_depth, gt_depth_original, crop_type, frame_index):
    current_dir = os.path.dirname(__file__)
    main_dir = current_dir.replace("/monodepth2", f"/output_data/crop_{crop_type}")
    output_file = f"resize_depth_plot_{frame_index}.png"
    output_path = os.path.join(main_dir, output_file)
    # Visualize the original and result images
    plt.figure(figsize=(25, 5))

    # Display the original image
    plt.subplot(1, 2, 1)
    plt.title("Original image")
    plt.imshow(gt_depth_original, cmap='gray')
    plt.axis('off')

    # Display the copped & resized image
    plt.subplot(1, 2, 2)
    plt.title("Cropped & Resized image")
    plt.imshow(gt_depth, cmap='gray')
    plt.axis('off')

    plt.savefig(output_path)

def save_image_to_file(data, file_name, type, crop_type, dataset_split):
    if dataset_split == "test_files":
        return
    
    if isinstance(data, torch.Tensor):
        data = data.cpu().numpy()

    print(f"Saving matrix: {file_name} | {type} | {crop_type}")
    crop_type_folder_path = f"output_data/crop_{crop_type}/raw_matrices"
    output_dir = os.path.join(MAIN_DIR, crop_type_folder_path)
    output_dir_type = os.path.join(output_dir, type)
    output_file = file_name + ".npz"
    output_path = os.path.join(output_dir_type, output_file)
    print("Output path: ", output_path)
    np.savez_compressed(output_path, data=np.array(data))

def convert_matrix_to_visualisation(disp, cmap_type="magma"):
    vmax = np.percentile(disp, 96)
    normalizer = mpl.colors.Normalize(vmin=disp.min(), vmax=vmax)
    mapper = cm.ScalarMappable(norm=normalizer, cmap=cmap_type)
    colormapped_im = (mapper.to_rgba(disp)[:, :, :3] * 255).astype(np.uint8)
    return colormapped_im
