#!/bin/bash

echo “initializing environment...”
# Load modules
ml JupyterNotebook/7.2.0-GCCcore-13.2.0
ml torchvision/0.18.0-foss-2023b-CUDA-12.4.0

# Init python virtual environment
source /home.nfs/gavulsim/sem_proj/bin/activate

# Switch to working directory
cd semestral_project/monodepth2
echo “done”