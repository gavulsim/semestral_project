import os
import skimage.transform
import numpy as np
import PIL.Image as pil
from PIL import Image
import torch
import cv2
import random

from torchvision import transforms

from .mono_dataset import MonoDataset
from utils import save_image_to_file

class CityscapesDataset(MonoDataset):
    def __init__(self, *args, **kwargs):
        super(CityscapesDataset, self).__init__(*args, **kwargs)

        # Additional Cityscapes-specific initialization if needed
        self.K = np.array([[2262.52, 0, 1096.98],
                           [0, 2265.301, 513.137],
                           [0, 0, 1]], dtype=np.float32)
        self.first_call = True

    def get_color(self, line, side, do_flip):
        image_name = line[2]
        original_color = self.loader(self.get_image_path(line))
        color = self.resize_color(original_color, self.first_call)

        if self.first_call == True:
            save_image_to_file(original_color, "input_original", self.dataset_type, self.crop_type, self.opt.dataset_split)
            save_image_to_file(color, "input_transformed", self.dataset_type, self.crop_type, self.opt.dataset_split)
            self.first_call = False

        if do_flip:
            color = color.transpose(Image.FLIP_LEFT_RIGHT)

        # print("Color shape: ", color.size)

        return color

    def get_image_path(self, line):
        first_folder = line[0]
        city_folder = line[1]
        image_name = line[2]
        
        # Construct the full image path
        image_path = os.path.join(
            self.data_path, 
            'leftImg8bit',
            first_folder, 
            city_folder, 
            f"{image_name}_leftImg8bit.jpg"
        )

        return image_path

    def get_depth(self, line, side, do_flip):
        first_folder = line[0]
        city_folder = line[1]
        image_name = line[2]
        
        # Construct the full depth path
        depth_path = os.path.join(
            self.data_path, 
            'disparity',
            first_folder, 
            city_folder, 
            f"{image_name}_disparity.png"
        )

        # Load depth maps
        depth_gt = np.array(Image.open(depth_path)).astype(np.float32)
        
        # Normalize depth values if necessary
        # You might need to adjust this based on how the depth values are stored
        depth_gt = depth_gt / 256.0

        if do_flip:
            depth_gt = np.fliplr(depth_gt)
        # print("Depth: ", depth_gt)
        return depth_gt
        
    def check_depth(self):
        # Return True if your Cityscapes dataset includes depth maps
        return True
    
    def __getitem__(self, index):
        inputs = {}

        do_color_aug = self.is_train and random.random() > 0.5
        do_flip = self.is_train and random.random() > 0.5

        line = self.filenames[index].split()
        
        side = None
        
        for i in self.frame_idxs:
            if i == "s":
                other_side = {"r": "l", "l": "r"}[side]
                inputs[("color", i, -1)] = self.get_color(line, other_side, do_flip)
            else:
                inputs[("color", i, -1)] = self.get_color(line, side, do_flip)

        # adjusting intrinsics to match each scale in the pyramid
        for scale in range(self.num_scales):
            K = self.K.copy()

            K[0, :] *= self.width // (2 ** scale)
            K[1, :] *= self.height // (2 ** scale)

            inv_K = np.linalg.pinv(K)

            inputs[("K", scale)] = torch.from_numpy(K)
            inputs[("inv_K", scale)] = torch.from_numpy(inv_K)

        if do_color_aug:
            color_aug = transforms.ColorJitter.get_params(
                self.brightness, self.contrast, self.saturation, self.hue)
        else:
            color_aug = (lambda x: x)

        self.preprocess(inputs, color_aug)

        for i in self.frame_idxs:
            del inputs[("color", i, -1)]
            del inputs[("color_aug", i, -1)]

        if self.load_depth:
            depth_gt = self.get_depth(line, side, do_flip)
            inputs["depth_gt"] = np.expand_dims(depth_gt, 0)
            inputs["depth_gt"] = torch.from_numpy(inputs["depth_gt"].astype(np.float32))

        if "s" in self.frame_idxs:
            stereo_T = np.eye(4, dtype=np.float32)
            baseline_sign = -1 if do_flip else 1
            side_sign = -1 if side == "l" else 1
            stereo_T[0, 3] = side_sign * baseline_sign * 0.1

            inputs["stereo_T"] = torch.from_numpy(stereo_T)

        inputs[("first_folder", 0)] = line[0]
        inputs[("city_folder", 0)] = line[1]
        inputs[("image_name", 0)] = line[2]
        return inputs
    