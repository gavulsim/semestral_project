import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

# Load the uploaded CSV file
current_dir = os.path.dirname(__file__)
output_dir = current_dir.replace("/monodepth2", "/output_data")

if __name__ == "__main__":
    errors_00_path = output_dir + "/crop_horizontal/raw_matrices/kitti/kitti_0.0_all_errors.csv"
    errors_02_path = output_dir + "/crop_horizontal/raw_matrices/kitti/kitti_0.2_all_errors.csv"
    # errors_00_path = output_dir + "/crop_horizontal/raw_matrices/cityscapes/cityscapes_0.0_all_errors.csv"
    # errors_02_path = output_dir + "/crop_horizontal/raw_matrices/cityscapes/cityscapes_0.2_all_errors.csv"
    error_data_00 = pd.read_csv(errors_00_path, header=None)
    error_data_02 = pd.read_csv(errors_02_path, header=None)

    abs_differences_values = np.abs(error_data_00[0].values - error_data_02[0].values)
    
    # print(abs_differences_values)

    abs_differences_values_np = np.array(abs_differences_values)

    min_value = np.min(abs_differences_values_np)
    argmin_value = np.argmin(abs_differences_values_np)
    max_value = np.max(abs_differences_values_np)
    argmax_value = np.argmax(abs_differences_values_np)

    print(f"Minimum value: {min_value} on index: {argmin_value+1}")
    print(f"Maximum value: {max_value} on index: {argmax_value+1}")