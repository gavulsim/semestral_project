import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd

START = 0.0
END = 0.22
# END = 0.02
STEP = 0.02
NUM = int(END / STEP)

current_dir = os.path.dirname(__file__)
output_dir = current_dir.replace("/monodepth2", "/output_data")

if __name__ == "__main__":
    file_range = np.linspace(START, END - 0.02, NUM)
    
    all_data = []

    for file_idx in file_range:
        file_path = f"{output_dir}/crop_horizontal/raw_matrices/cityscapes/cityscapes_{file_idx}_all_errors.csv"
        df = pd.read_csv(file_path)
        data = [value[0] for value in df.values.tolist()]
        all_data.append(data)
    
    plt.figure(figsize=(12, 6))
    plt.boxplot(all_data, patch_artist=True)

    plt.xticks(range(0, (NUM+1)), ["Crop"] + [f"Crop {i}" for i in file_range])
    plt.title(f"Boxplot for cityscapes dataset ({NUM} crops).")
    plt.ylabel("Values")

    # Save the box plot
    plt.savefig(f"{output_dir}/crop_horizontal/raw_matrices/cityscapes/boxplot_cityscapes.png", bbox_inches='tight', dpi=300)

