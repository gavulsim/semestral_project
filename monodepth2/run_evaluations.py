import subprocess
import numpy as np
import pandas as pd
import os
import csv
import argparse


from utils import Datasets

import matplotlib.pyplot as plt
from matplotlib import colors, cm
from skimage.measure import regionprops

# Dataset settings
KITTI_DATASET = {
    "name": "kitti",
    "export_script": "python export_gt_depth.py --data_path /mnt/datasets/kitti_data --split eigen --dataset_type kitti",
    "eval_script": "python evaluate_depth.py --load_weights_folder models/mono_640x192 --eval_mono --data_path /mnt/datasets/kitti_data --dataset_type kitti"
}

CITY_DATASET = {
    "name": "cityscapes",
    "export_script": "python export_gt_depth.py --data_path /mnt/datasets/cityscapes --split city --dataset_type cityscapes",
    "eval_script": "python evaluate_depth.py --load_weights_folder models/mono_640x192 --eval_mono --data_path /mnt/datasets/cityscapes --eval_split city --dataset_type cityscapes"
}

# Loop settings
START = 0.0
END = 0.22
# END = 0.06
STEP = 0.02
# STEP = 0.22
NUM = int(END / STEP)

current_dir = os.path.dirname(__file__)
output_dir = current_dir.replace("/monodepth2", "/output_data")

def create_boxplot(dataset_type, crop_type, dataset_split):
    if dataset_split == "test_file":
        return

    file_range = np.linspace(START, END - 0.02, NUM)
    
    all_data = []

    for file_idx in file_range:
        file_path = f"{output_dir}/crop_{crop_type}/raw_matrices/{dataset_type}/{dataset_type}_{file_idx}_all_errors.csv"
        df = pd.read_csv(file_path)
        data = [value[0] for value in df.values.tolist()]
        all_data.append(data)

    labels = [f"Crop {i:.2f}" for i in file_range]   
    plt.figure(figsize=(12, 6))
    plt.boxplot(all_data, patch_artist=True, tick_labels=labels)

    offset = 0.01

    # Calculate statistics
    for i, dataset in enumerate(all_data):
        # 25th percentile (Q1)
        q1 = np.percentile(dataset, 25) 
        median = np.median(dataset)
        # 75th percentile (Q3)
        q3 = np.percentile(dataset, 75)
        min_val = np.min(dataset)
        max_val = np.max(dataset)
        iqr = q3 - q1
        # Lower fence
        lower_fence = max(min(dataset), q1 - 1.5 * iqr)
        # Upper fence
        upper_fence = min(max(dataset), q3 + 1.5 * iqr)

        # Annotate the values on the graph
        plt.text(i + 1, lower_fence + offset, f"{lower_fence:.3f}", ha="center", va="top", fontsize=8, color="blue", fontweight="bold")
        plt.text(i + 1, q1 - offset, f"{q1:.3f}", ha="center", va="center", fontsize=8, color="blue", fontweight="bold")
        plt.text(i + 1, median + offset, f"{median:.3f}", ha="center", va="center", fontsize=8, color="red", fontweight="bold")
        plt.text(i + 1, q3 + offset, f"{q3:.3f}", ha="center", va="center", fontsize=8, color="blue", fontweight="bold")
        plt.text(i + 1, upper_fence - offset, f"{upper_fence:.3f}", ha="center", va="bottom", fontsize=8, color="blue", fontweight="bold")

    plt.title(f"Boxplot for {dataset_type} dataset ({NUM} crops).")
    plt.ylabel("Values")
    plt.xlabel("Crop")

    # Save the box plot
    print(f"Saving box plot for {dataset_type} dataset")
    plt.savefig(f"{output_dir}/crop_{crop_type}/raw_matrices/{dataset_type}/boxplot_{dataset_type}.png", bbox_inches='tight', dpi=300)

def save_visualization_error_matrices(path, name, dataset_type, dataset_split):
    if dataset_split == "test_files":
        return

    matrices = []
    errors = []

    matrix_range = np.linspace(START, END - 0.02, NUM)

    all_vmax = []

    for i in matrix_range:
        rounded_i = np.round(i, 2)
        matrix_path = f"{path}/error_matrices/{name}_{rounded_i}.npz"
        err_matrix = np.load(matrix_path, fix_imports=True, encoding='latin1')["data"]
        vmax = np.percentile(err_matrix, 96)
        all_vmax.append(vmax)
        matrices.append(err_matrix)
        valid_error_values = err_matrix[err_matrix != -1]
        error_mean = np.mean(valid_error_values)
        errors.append(error_mean)

    titles = [f"Error matrix with crop: {i}, error: {round(err, 3):.3f}" for (i, err) in zip(matrix_range, errors)]

    ncols = 4
    nrows = (NUM + ncols - 1) // ncols

    fig, axes = plt.subplots(nrows=nrows, ncols=ncols, figsize=(15, 10))

    axes = axes.flatten()

    vmin = 0
    vmax = max(all_vmax)

    for i, (matrix, ax) in enumerate(zip(matrices, axes)):
        normalizer = colors.Normalize(vmin=vmin, vmax=vmax)
        mapper = cm.ScalarMappable(norm=normalizer, cmap="magma_r")

        img = ax.imshow(matrix, cmap="magma_r", norm=normalizer)
        ax.set_title(titles[i], fontsize=10)
        ax.axis("off")

        cbar = fig.colorbar(mapper, ax=ax, orientation="vertical", shrink=0.8, pad=0.05)
        cbar.set_label("Value", fontsize=8)
        cbar.set_ticks(np.linspace(vmin, vmax, num=5))
        cbar.set_ticklabels([f"{val:.2f}" for val in np.linspace(vmin, vmax, num=5)])

    for ax in axes[NUM:]:
        ax.axis("off")

    plt.tight_layout()
    plt.savefig(f"{path}/{dataset_type}_{name}.png", bbox_inches='tight', dpi=300)
    plt.close(fig)

def save_all_images(path, crop_factor, dataset_type, dataset_split):
    if dataset_split == "test_files":
        return

    input_original = np.load(f"{path}/input_original.npz", fix_imports=True, encoding='latin1')["data"]
    input_transformed = np.load(f"{path}/input_transformed.npz", fix_imports=True, encoding='latin1')["data"]
    gt_depth_original = np.load(f"{path}/gt_depth_original.npz", fix_imports=True, encoding='latin1')["data"]
    gt_depth = np.load(f"{path}/gt_depth.npz", fix_imports=True, encoding='latin1')["data"]
    pred_depth = np.load(f"{path}/pred_depth_scaled.npz", fix_imports=True, encoding='latin1')["data"]
    error_matrix = np.load(f"{path}/error_matrix.npz", fix_imports=True, encoding='latin1')["data"]
    crop_mask = np.load(f"{path}/mask_matrix.npz", fix_imports=True, encoding='latin1')["data"]

    images = [input_original, input_transformed, gt_depth_original, gt_depth, pred_depth, error_matrix]
    titles = ["Input Original", "Input Transformed", "GT Depth Original", "GT Depth", "Pred Depth", "Error matrix"]

    fig, axes = plt.subplots(len(titles), 1, figsize=(8, 20))

    # Region to mark max crop from crop mask
    regions = regionprops(crop_mask.astype(int))
    if regions:
        min_row, min_col, max_row, max_col = regions[0].bbox

    for i, ax in enumerate(axes):
        image = images[i]
        if i > 1:
            vmin = 0
            vmax = np.percentile(image, 97)
            normalizer = colors.Normalize(vmin=vmin, vmax=vmax)
            mapper = cm.ScalarMappable(norm=normalizer, cmap="magma_r")
            image = (mapper.to_rgba(image)[:, :, :3] * 255).astype(np.uint8)
            # Add a color bar with the recreated colormap and normalization
            cbar = plt.colorbar(cm.ScalarMappable(norm=normalizer, cmap=cm.magma_r), ax=ax)
            cbar.set_label("Value")

            # Set specific numeric tick labels on the color bar
            tick_values = np.linspace(vmin, vmax, num=5)
            cbar.set_ticks(tick_values)
            cbar.set_ticklabels([f"{val:.2f}" for val in tick_values])

        ax.imshow(image, cmap="gray")
        ax.set_title(titles[i])
        if i > 2:
            rect = plt.Rectangle(
                (min_col, min_row),
                max_col - min_col,
                max_row - min_row,
                linewidth=2,
                edgecolor="red",
                facecolor="none"
            )
            ax.add_patch(rect)
        ax.axis('off')

    plt.subplots_adjust(hspace=0.1, top=0.95, bottom=0.05)

    plt.savefig(f"{path}/{dataset_type}_comparison_{crop_factor}.png", bbox_inches='tight', dpi=300)
    plt.close(fig)

def run_eval():
    parser = argparse.ArgumentParser(description='run evaluation')

    parser.add_argument("--dataset_type",
                        type=str,
                        help="type of dataset to evaluate",
                        required=True,
                        choices=["kitti", "cityscapes"])
    parser.add_argument("--crop_type",
                        type=str,
                        help="Crop type if crop from top and bottom or left and right",
                        required=True,
                        choices=["horizontal", "vertical"])
    parser.add_argument("--dataset_split",
                        type=str,
                        help="determine which evaluation will be performed",
                        required=False,
                        default="test_files",
                        choices=["test_files", "test_file"])
    opt = parser.parse_args()

    if opt.dataset_type == Datasets.KITTI.value:
        dataset = KITTI_DATASET
        print("Evaluating KITTI dataset")
    elif opt.dataset_type == Datasets.CITY.value:
        dataset = CITY_DATASET
        print("Evaluating Cityscapes dataset")
    # Create and write header to file
    file_error = f"{dataset['name']}_errors.csv"
    output_dir_crop = os.path.join(output_dir, f"crop_{opt.crop_type}")
    output_matrices_path = f"{output_dir_crop}/raw_matrices/{opt.dataset_type}"

    if opt.dataset_split == "test_files":
        error_path = os.path.join(output_dir_crop, file_error)
    else:
        error_path = os.path.join(output_matrices_path, file_error)

    with open(error_path, mode='w', newline='') as file:
        writer = csv.writer(file)
        writer.writerow(["crop_factor", "crop_rel_abs", "abs_rel", "sq_rel", "rmse", "rmse_log", "a1", "a2", "a3"])

    for crop_factor in np.arange(START, END, STEP):
        print(f"Running with crop_factor = {crop_factor}")
        
        export_command = dataset["export_script"] + f" --dataset_split {opt.dataset_split} --crop_factor {crop_factor} --crop_type {opt.crop_type}"
        subprocess.run(export_command, shell=True, check=True)
        
        evaluate_command = dataset["eval_script"] + f" --dataset_split {opt.dataset_split} --crop_factor {crop_factor} --crop_type {opt.crop_type}"
        subprocess.run(evaluate_command, shell=True, check=True)
        save_all_images(output_matrices_path, crop_factor, opt.dataset_type, opt.dataset_split)

    save_visualization_error_matrices(output_matrices_path, "error_matrix_relative", opt.dataset_type, opt.dataset_split)
    save_visualization_error_matrices(output_matrices_path, "error_matrix_relative_crop", opt.dataset_type, opt.dataset_split)
    create_boxplot(opt.dataset_type, opt.crop_type, opt.dataset_split)

if __name__ == "__main__":
    run_eval()
