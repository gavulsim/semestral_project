# Copyright Niantic 2019. Patent Pending. All rights reserved.
#
# This software is licensed under the terms of the Monodepth2 licence
# which allows for non-commercial use only, the full terms of which are made
# available in the LICENSE file.

from __future__ import absolute_import, division, print_function

import os

import argparse
import numpy as np
import PIL.Image as pil
import cv2
import pickle
import torch
import torchvision

from utils import Datasets
from utils import readlines
from utils import crop_gt_depth
from utils import resize_gt_depth
from utils import convert_disp_to_depth
from utils import save_image_to_file
from kitti_utils import generate_depth_map


def export_gt_depths_kitti():

    parser = argparse.ArgumentParser(description='export_gt_depth')

    parser.add_argument('--data_path',
                        type=str,
                        help='path to the root of the KITTI data',
                        required=True)
    parser.add_argument('--split',
                        type=str,
                        help='which split to export gt from',
                        required=True,
                        choices=["eigen", "eigen_benchmark", "city"])
    parser.add_argument("--dataset_type",
                            type=str,
                            help='exporting data from KITTI or cityscapes dataset',
                            required=True,
                            choices=["kitti", "cityscapes"])
    parser.add_argument("--dataset_split",
                        type=str,
                        help='dataset from which day export',
                        required=False,
                        choices=["test_files26", "test_files28", "test_files29", "test_files30", "test_files03", "test_files", "test_file"])
    parser.add_argument('--crop_factor',
                        type=float,
                        help='crop factor that determines how much the image will be cropped',
                        required=False,
                        default=0)
    parser.add_argument("--crop_type",
                        type=str,
                        help="crop type determined how image will be cropped",
                        required=False,
                        default="horizontal",
                        choices=["horizontal", "vertical"])
    opt = parser.parse_args()

    # Split folder:  /home.nfs/gavulsim/monodepth2/splits/eigen
    split_folder = os.path.join(os.path.dirname(__file__), "splits", opt.split)
    # open the file with file/folders names --> 2011_09_26/2011_09_26_drive_0002_sync 0000000069 l
    dataset_file_name = opt.dataset_split + ".txt"
    # dataset_file_name = "test_files.txt"
    lines = readlines(os.path.join(split_folder, dataset_file_name))
    print("Exporting ground truth depths for {}".format(opt.split))

    gt_depths = []
    for i, line in enumerate(lines):
        if opt.dataset_type == Datasets.KITTI.value:
            folder, frame_id, _ = line.split()
            frame_id = int(frame_id)
        elif opt.dataset_type == Datasets.CITY.value:
            folder_data, folder_city, name = line.split()
        
        if opt.split == "eigen":
            gt_depth_path = os.path.join(opt.data_path, folder, "proj_depth",
                                         "groundtruth", "image_02", "{:010d}.npz".format(frame_id))
            gt_depth_original = np.load(gt_depth_path, fix_imports=True, encoding='latin1')["data"]
            # gt_depth_original = np.array(pil.open(gt_depth_path)).astype(np.float32) / 256
            # calib_dir = os.path.join(opt.data_path, folder.split("/")[0])
            # velo_filename = os.path.join(opt.data_path, folder, "velodyne_points/data", "{:010d}.bin".format(frame_id))
            # gt_depth_original = generate_depth_map(calib_dir, velo_filename, 2, True)

        elif opt.split == "eigen_benchmark":
            # Path to the ground truth file
            gt_depth_path = os.path.join(opt.data_path, folder, "proj_depth",
                                         "groundtruth", "image_02", "{:010d}.png".format(frame_id))
            # Open ground truth file
            gt_depth_original = np.array(pil.open(gt_depth_path)).astype(np.float32) / 256
        
        elif opt.split == "city":
            gt_depth_path = os.path.join(opt.data_path, "disparity", folder_data, folder_city, f"{name}_disparity.png")
            # Load disparity maps
            img_d = cv2.imread(gt_depth_path, cv2.IMREAD_UNCHANGED).astype(np.float32)
            # Convert disparity maps to depth map        
            gt_depth_original = convert_disp_to_depth(img_d)

        gt_depth, size = crop_gt_depth(gt_depth_original, opt.crop_factor, opt.crop_type, i)
        # gt_depth = resize_gt_depth(gt_depth, size, i)

        if i == 0:
            save_image_to_file(gt_depth_original, "gt_depth_original", opt.dataset_type, opt.crop_type, opt.dataset_split)
            save_image_to_file(gt_depth, "gt_depth", opt.dataset_type, opt.crop_type, opt.dataset_split)
        
        gt_depths.append((gt_depth.astype(np.float32), gt_depth_original.shape))

    # print(f"Line {line} has length {len(gt_depth.astype(np.float32))} / {len(gt_depth[0].astype(np.float32))}")

    # output_path = os.path.join(split_folder, "gt_depths.npz")
    output_path = os.path.join(split_folder, "gt_depths.pkl")

    print("Saving to {}".format(opt.split))
    
    # np.savez_compressed(output_path, data=np.array(gt_depths))
    with open(output_path, 'wb') as f:
        pickle.dump(gt_depths, f)


if __name__ == "__main__":
    export_gt_depths_kitti()
